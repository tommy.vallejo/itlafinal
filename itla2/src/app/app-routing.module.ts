import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExpenseComponent } from './Records/ExpenseReport/expense/expense.component';
import { IBListComponent } from './Records/IncomeBillsList/iblist/iblist.component';
import { IncomeComponent } from './Records/IncomeReport/income/income.component';
import { IncomeTableComponent } from './Tables/income-table/income-table.component';

const routes: Routes = [
  {path:'', redirectTo:'income-table', pathMatch:'full'},
  {path:'iblist', component: IBListComponent},
  {path: 'expense', component: ExpenseComponent},
  {path: 'income', component: IncomeComponent},
  {path: 'income-table', component: IncomeTableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
