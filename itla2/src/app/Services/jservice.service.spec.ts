import { TestBed } from '@angular/core/testing';

import { JserviceService } from './jservice.service';

describe('JserviceService', () => {
  let service: JserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
