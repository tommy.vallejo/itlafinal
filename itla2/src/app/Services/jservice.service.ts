import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../Interfaces/product';


@Injectable({
  providedIn: 'root'
})
export class JserviceService {



  constructor(private http: HttpClient) { }

  getIncomeList(): Observable<Product[]>/*This method retur a object array with gorcery product from Json Server on Port: 3000*/
  {
    let url: string = 'http://localhost:3000/';
    return this.http.get<Product[]>(url + "expense2");
  

  }
  postExpenseT(date){
    let body ={
      
        id: date.value.Transnumber,
        TranNumber: date.value.Transnumber,
        Bank: date.value.Addressee,
        Date: date.value.Date,
        Amount: date.value.Amount,
        Tipo: date.value.Tipo,
      
    }

    let url: string = 'http://localhost:3000/expense2';
    console.warn(body);
    console.warn(url);
    return this.http.post<Product[]>(url, body);

  }
}
