import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExpenseComponent } from './Records/ExpenseReport/expense/expense.component';

import { IBListComponent } from './Records/IncomeBillsList/iblist/iblist.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './Navigation/NavBar/nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import {MatDialogModule} from '@angular/material/dialog';

import {FormsModule, ReactiveFormsModule} from '@angular/forms'








import { MatSliderModule } from '@angular/material/slider';


import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';

import { CdkTableModule } from '@angular/cdk/table';
import { MatSortModule } from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';


import {MatGridListModule} from '@angular/material/grid-list';
import { IncomeComponent } from './Records/IncomeReport/income/income.component';
import { MatTableModule } from '@angular/material/table';
import { IncomeTableComponent } from './Tables/income-table/income-table.component';


import { ToastrModule } from 'ngx-toastr';




@NgModule({
  declarations: [
    AppComponent,
    ExpenseComponent,
    IBListComponent,
    NavComponent,
    IncomeComponent,
    IncomeTableComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    CdkTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatGridListModule,
    MatTableModule,
    MatDialogModule,
    ToastrModule.forRoot(),
  ],
  exports:[BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    CdkTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatGridListModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
