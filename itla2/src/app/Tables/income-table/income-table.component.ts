import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { filter } from 'rxjs';
import { Product } from 'src/app/Interfaces/product';
import { JserviceService } from 'src/app/Services/jservice.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-income-table',
  templateUrl: './income-table.component.html',
  styleUrls: ['./income-table.component.css']
})
export class IncomeTableComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;


  dcData:Product[] =[];
  searchKey: string;

  displayedColumns: string[] = ['TranNumber', 'Bank', 'Date', 'Amount', 'Tipo'];
  data:MatTableDataSource<Product[]>;
  @ViewChild('paginator') paginator: MatPaginator;

  constructor(private service: JserviceService){

  }

  reloadData(){
    this.service.getIncomeList().subscribe((res:any) => {
      
      this.data.paginator = this.paginator;
      this.dcData = res;




    });

  }

  ngOnInit(): void {

    this.service.getIncomeList().subscribe((res:any) => {
      this.data = new MatTableDataSource(res);
      this.data = new MatTableDataSource(res);
      console.warn(res);
      this.data.paginator = this.paginator;
      this.dcData = res;
      this.data.sort = this.sort;




    });

  }

  applyFilter(filterValue: string){
    this.data.filter = filterValue.trim().toLowerCase();

  }

  onChange($event:any){
    let filteredData = _.filter(this.dcData,(item) =>{

      
      return item.Tipo ==  $event.value;
      
    })

   
    this.data = new MatTableDataSource(filteredData);

  }

  }
 

