import { Component, OnInit } from '@angular/core';
import { JserviceService } from 'src/app/Services/jservice.service';

@Component({
  selector: 'app-iblist',
  templateUrl: './iblist.component.html',
  styleUrls: ['./iblist.component.css']
})
export class IBListComponent implements OnInit {

  data:any;

  constructor(private serv:JserviceService) { }



  ngOnInit(): void {

    this.serv.getIncomeList().subscribe(res => {
      this.data = res;
      console.log(res);
      console.warn("Hello!");

    });
  }

}
