import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IBListComponent } from './iblist.component';

describe('IBListComponent', () => {
  let component: IBListComponent;
  let fixture: ComponentFixture<IBListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IBListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IBListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
