import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { JserviceService } from 'src/app/Services/jservice.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.css']
})
export class ExpenseComponent implements OnInit {
  addressForm:FormGroup
  @ViewChild('date') private date: NgForm;

  constructor(private fb: FormBuilder, private service: JserviceService,private toastr: ToastrService) { 
    this.buildForm()
    
  }

  private buildForm(){

    this.addressForm = this.fb.group({
      Transnumber: [null, Validators.required],
      Addressee: [null, Validators.required],
      Date: [null, Validators.required],
    Amount: [null, Validators.required],
    Tipo: [null, Validators.required]
  })
 }


  Data(d: NgForm){
    if(this.addressForm.valid){

    console.log(d.value);
    console.log(d.valid);
    this.service.postExpenseT(d).subscribe((resp:any) => { 
      if(resp.succeeded === true){
        this.toastr.success('Registered!', 'Registration success.');
      } 
       else{
        err => {
          if (err.status === 404 || err.status === 500  || err.status === 405  || err.status === 0){
            this.toastr.error('Invalid Registration', 'Failed.!');
            console.warn(err);

          }

        }
      }
     });
     this.date.resetForm();
     
    }

    }

  ngOnInit(): void {
  }

}
